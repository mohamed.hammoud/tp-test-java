package Jauge;

public class JaugeNegatif implements Jauge {
 public long valeur;
 public final long min;
 public final long max;
 
 public JaugeNegatif(long vigieMin, long vigieMax, long depart) {
   valeur = -depart;
   min = -vigieMin;
   max = -vigieMax;
  
 }

 public boolean estRouge() {
   return Math.abs(valeur) >= Math.abs(max);
 }

 public boolean estVert() {
   return (!estBleu() && !estRouge());
 }
 
 public boolean estBleu() {
   return Math.abs(valeur) <= Math.abs(min);
 }

 public void incrementer() {
	 if(valeur >= 0) {
		 valeur ++;
	 }
	 else {
		 valeur --;
	 }
	 
 }
 
 public void decrementer() {
	 if(valeur >= 0) {
		 valeur --;
	 }
	 else {
		 valeur ++;
	 }
 }

 @Override
 public String toString() {
   return "<" + valeur + " [" + min + "," + max + "]>";
 }
}

