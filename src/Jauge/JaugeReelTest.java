package Jauge;


import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;



public class JaugeReelTest implements JaugeTest {
	JaugeReel jaugeDsInter;
	JaugeReel jaugeInf;
	JaugeReel jaugeSup;
	JaugeReel  jaugeLimiteViggie;
	JaugeReel  jaugeMaxEqMin;

	
	@Override
	public Jauge creerJauge(long i, long j, long val) {
		return new JaugeReel(i,j,val);
	}

	@Before
	public void setUp() throws Exception {
		
		jaugeDsInter = new JaugeReel(4,6,5);
		jaugeInf = new JaugeReel(4,6,2);
		jaugeSup = new JaugeReel(4,6,7);
		jaugeLimiteViggie = new JaugeReel(8,4,6);
		jaugeMaxEqMin = new JaugeReel(8,8,6);


	}

	@After
	public void tearDown() throws Exception {
		jaugeDsInter = null;
	}

	
	public void testDansIntervalle() {
		assertFalse(jaugeDsInter.estRouge());
		assertFalse(jaugeDsInter.estBleu());
		assertTrue(jaugeDsInter.estVert());
	}
	

    public void testInferieurIntervalle() {
        assertTrue(jaugeInf.estBleu());
        assertFalse(jaugeInf.estVert());
        assertFalse(jaugeInf.estRouge());
        
    }
    
 
    public void testSuperieurIntervalle() {
        assertFalse(jaugeSup.estBleu());
        assertFalse(jaugeSup.estVert());
        assertTrue(jaugeSup.estRouge());
    }
    
    @Test
    public void testDeplacement() {
    	jaugeDsInter.decrementer();
        assertFalse(jaugeDsInter.estRouge());
        assertTrue(jaugeDsInter.estBleu());
        assertFalse(jaugeDsInter.estVert());
        
        jaugeDsInter.incrementer();
        assertFalse(jaugeDsInter.estRouge());
        assertFalse(jaugeDsInter.estBleu());
        assertTrue(jaugeDsInter.estVert());
    }

  
    public void testLimiteVigieMaxInferieurVigieMin() {
    	assertNotNull(jaugeLimiteViggie);
    	
    }

    public void testMaxEgaleMin() {
    	assertNotNull(jaugeMaxEqMin);
    	
    }
    

    @Test
   public void run() {
	   testDansIntervalle() ;
	   testMaxEgaleMin();
	   testLimiteVigieMaxInferieurVigieMin();
	   testDeplacement();
	   testSuperieurIntervalle();
	   testInferieurIntervalle();
	   
	   
	   
	   
   }
    

}
