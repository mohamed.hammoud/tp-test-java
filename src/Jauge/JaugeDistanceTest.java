package Jauge;


import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class JaugeDistanceTest {
    JaugeDistance jaugeDansIntervalle;
    JaugeDistance jaugeInferieurIntervalle;
    JaugeDistance jaugeSuperieurIntervalle;
    JaugeDistance jaugeLimitVigieMax;
    JaugeDistance jaugeMaxEgalMin;
    JaugeDistance jaugeSuperieur;
    @Before
    public void setUp() throws Exception {
        
        jaugeDansIntervalle = new JaugeDistance(10,20,11);
        jaugeInferieurIntervalle = new JaugeDistance(10,20,10);
        jaugeSuperieurIntervalle = new JaugeDistance(10,20,30);
        jaugeLimitVigieMax = new JaugeDistance(20,10,15);
        jaugeMaxEgalMin = new JaugeDistance(15,15,10);
        jaugeSuperieur = new JaugeDistance(15,15,20);
    }
    @After
    public void tearDown() throws Exception {
        jaugeDansIntervalle = null;
    }
    public void testDansIntervalle() {
        assertFalse(jaugeDansIntervalle.estRouge());
        assertFalse(jaugeDansIntervalle.estBleu());
        assertTrue(jaugeDansIntervalle.estVert());
    }
    public void testInferieurIntervalle() {
        assertTrue(jaugeInferieurIntervalle.estBleu());
        assertFalse(jaugeInferieurIntervalle.estVert());
        assertFalse(jaugeInferieurIntervalle.estRouge());
        
    }
    public void testSuperieurIntervalle() {
        assertFalse(jaugeSuperieurIntervalle.estBleu());
        assertFalse(jaugeSuperieurIntervalle.estVert());
        assertTrue(jaugeSuperieurIntervalle.estRouge());
    }
    
    public void testDeplacement() {
        jaugeDansIntervalle.decrementer();
        jaugeDansIntervalle.decrementer();
        assertFalse(jaugeDansIntervalle.estRouge());
        assertTrue(jaugeDansIntervalle.estBleu());
        assertFalse(jaugeDansIntervalle.estVert());
        
        jaugeDansIntervalle.incrementer();
        jaugeDansIntervalle.incrementer();
        assertFalse(jaugeDansIntervalle.estRouge());
        assertFalse(jaugeDansIntervalle.estBleu());
        assertTrue(jaugeDansIntervalle.estVert());
    }
    
    public void testLimiteVigieMaxInferieurVigieMin() {
        assertNotNull(jaugeLimitVigieMax);
    }
    
    public void testMaxEgalMin() {
        assertNotNull(jaugeMaxEgalMin);
    }
    
    @Test
    public void run() {
        testDansIntervalle();
        testSuperieurIntervalle();
        testInferieurIntervalle();
        testDeplacement();
        testLimiteVigieMaxInferieurVigieMin();
        testMaxEgalMin();
        
    }
}