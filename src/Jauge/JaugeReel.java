package Jauge;


public class JaugeReel implements Jauge {
 public float valeur;
 public final float min;
 public final float max;

 public JaugeReel(float vigieMin, float vigieMax, float depart) {
   valeur = depart/1000;
   min = vigieMin/1000;
   max = vigieMax/1000;
 
 }

 @Override
 public boolean estRouge() {
   return valeur >= max;
 }

 @Override
 public boolean estVert() {
   return (!estBleu() && !estRouge());
 }

 @Override
 public boolean estBleu() {
   return valeur <= min;
 }

 @Override
 public void incrementer() {
   valeur++;
 }

 @Override
 public void decrementer() {
	 valeur --;
 }

 @Override
 public String toString() {
   return "<" + valeur + " [" + min + "," + max + "]>";
 }
}
