package EtatPassager;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EtatPassagerChaineTest implements IEtatPassagerTest {
	EtatPassagerChaine etatPassagerAssis;
	EtatPassagerChaine etatPassagerDebout;
	EtatPassagerChaine etatPassagerDehors;
	
	public IEtatPassager creerAssis() {
	    return new EtatPassagerChaine("ASSIS");
	}

	public IEtatPassager creerDehors() {
		 return new EtatPassagerChaine("DEHORS");
	}

	public IEtatPassager creerDebout() {
		 return new EtatPassagerChaine("DEBOUT");
	}

	
	@Before
	public void setUp() throws Exception {
		etatPassagerAssis = new EtatPassagerChaine("ASSIS");
		etatPassagerDebout = new EtatPassagerChaine("DEBOUT");
		etatPassagerDehors = new EtatPassagerChaine("DEHORS");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEtatPassager() {
		assertNotNull(etatPassagerAssis);
		assertNotNull(etatPassagerDebout);
		assertNotNull(etatPassagerDehors);
	}

	@Test
	public void testEstExterieur() {
		assertTrue(etatPassagerDehors.estExterieur());
		assertFalse(etatPassagerDehors.estAssis());
		assertFalse(etatPassagerDehors.estDebout());
}

	@Test
	public void testEstAssis() {
		assertTrue(etatPassagerAssis.estAssis());
		assertFalse(etatPassagerAssis.estExterieur());
		assertFalse(etatPassagerAssis.estDebout());
	}

	@Test
	public void testEstDebout() {
		assertTrue(etatPassagerDebout.estDebout());
		assertFalse(etatPassagerDebout.estExterieur());
		assertFalse(etatPassagerDebout.estAssis());
	}

	@Test
	public void testEstInterieur() {
		assertTrue(etatPassagerAssis.estInterieur());
		assertTrue(etatPassagerDebout.estInterieur());
		assertFalse(etatPassagerDehors.estInterieur());
	}

	


}