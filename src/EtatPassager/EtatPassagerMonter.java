package EtatPassager;

public class EtatPassagerMonter {
	  
	  public enum Etat {ASSIS,  DEBOUT};

	  private final Etat monEtat;


	  public EtatPassagerMonter(Etat e) {
		  this.monEtat = e;
	  }

	  public boolean estAssis() {
		  return this.monEtat == Etat.ASSIS;
	  }

	 
	  public boolean estDebout() {
		  return this.monEtat == Etat.DEBOUT;
	  }

	  public boolean estInterieur() {
		  return this.monEtat == Etat.DEBOUT || this.monEtat == Etat.ASSIS  ;
	  }

	  @Override
	  public String toString() {
		  return "<" + monEtat + ">";
	  }
} 


