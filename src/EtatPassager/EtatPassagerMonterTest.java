package EtatPassager;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EtatPassagerMonterTest  {
	EtatPassagerMonter etatPassagerAssis;
	EtatPassagerMonter etatPassagerDebout;


	@Before
	public void setUp() throws Exception {
		
		this.etatPassagerAssis = new EtatPassagerMonter(EtatPassagerMonter.Etat.ASSIS);
		this.etatPassagerDebout = new EtatPassagerMonter(EtatPassagerMonter.Etat.DEBOUT);

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEtatPassager() {
		assertNotNull(etatPassagerAssis);
		assertNotNull(etatPassagerDebout);
	
	}


	@Test
	public void testEstAssis() {
		assertTrue(etatPassagerAssis.estAssis());
		assertFalse(etatPassagerAssis.estDebout());
	}

	@Test
	public void testEstDebout() {
		assertTrue(etatPassagerDebout.estDebout());
		assertFalse(etatPassagerDebout.estAssis());
	}

	@Test
	public void testEstInterieur() {
		assertTrue(etatPassagerAssis.estInterieur());
		assertTrue(etatPassagerDebout.estInterieur());

	}


}
