package EtatPassager;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EtatPassagerTest implements IEtatPassagerTest {
	EtatPassager etatPassagerAssis;
	EtatPassager etatPassagerDebout;
	EtatPassager etatPassagerDehors;
	
	public IEtatPassager creerAssis() {
	    return new EtatPassager(EtatPassager.Etat.ASSIS);
	}

	public IEtatPassager creerDehors() {
	    return new EtatPassager(EtatPassager.Etat.DEHORS);
	}

	public IEtatPassager creerDebout() {
	    return new EtatPassager(EtatPassager.Etat.DEBOUT);
	}

	@Before
	public void setUp() throws Exception {
		
		this.etatPassagerAssis = new EtatPassager(EtatPassager.Etat.ASSIS);
		this.etatPassagerDebout = new EtatPassager(EtatPassager.Etat.DEBOUT);
		this.etatPassagerDehors = new EtatPassager(EtatPassager.Etat.DEHORS);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEtatPassager() {
		assertNotNull(etatPassagerAssis);
		assertNotNull(etatPassagerDebout);
		assertNotNull(etatPassagerDehors);
	}

	@Test
	public void testEstExterieur() {
		assertTrue(etatPassagerDehors.estExterieur());
		assertFalse(etatPassagerDehors.estAssis());
		assertFalse(etatPassagerDehors.estDebout());
}

	@Test
	public void testEstAssis() {
		assertTrue(etatPassagerAssis.estAssis());
		assertFalse(etatPassagerAssis.estExterieur());
		assertFalse(etatPassagerAssis.estDebout());
	}

	@Test
	public void testEstDebout() {
		assertTrue(etatPassagerDebout.estDebout());
		assertFalse(etatPassagerDebout.estExterieur());
		assertFalse(etatPassagerDebout.estAssis());
	}

	@Test
	public void testEstInterieur() {
		assertTrue(etatPassagerAssis.estInterieur());
		assertTrue(etatPassagerDebout.estInterieur());
		assertFalse(etatPassagerDehors.estInterieur());
	}


}
