package EtatPassager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public interface IEtatPassagerTest  {

	public IEtatPassager creerAssis();

	public IEtatPassager creerDehors();

	public IEtatPassager creerDebout();
	
	@Before
	public void setUp() throws Exception;

	@After
	public void tearDown() throws Exception;

	@Test
	public void testEtatPassager();

	@Test
	public void testEstExterieur();
	
	@Test
	public void testEstAssis();

	@Test
	public void testEstDebout();

	@Test
	public void testEstInterieur();

}
