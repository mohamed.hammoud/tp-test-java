package EtatPassager;
public class EtatPassager implements IEtatPassager {
  public enum Etat {ASSIS,  DEBOUT,  DEHORS};

  private final Etat monEtat;

  
  public EtatPassager(Etat e) {
    this.monEtat = e;
  }

  @Override
  public boolean estExterieur() {
	  return this.monEtat == Etat.DEHORS;
  }

  @Override
  public boolean estAssis() {
    return this.monEtat == Etat.ASSIS;
  }

  @Override
  public boolean estDebout() {
	  return this.monEtat == Etat.DEBOUT;
  }

  @Override
  public boolean estInterieur() {
    return this.monEtat != Etat.DEHORS;
  }

  @Override
  public String toString() {
    return "<" + monEtat + ">";
  }
}