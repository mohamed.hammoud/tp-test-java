package EtatPassager;

public class EtatPassagerChaine implements IEtatPassager {
	
private final String monEtat;


public EtatPassagerChaine(String etat) {
	this.monEtat = etat;
}


public boolean estExterieur() {
	return this.monEtat == "DEHORS";
}


public boolean estAssis() {
	return this.monEtat == "ASSIS";
}


public boolean estDebout() {
	return this.monEtat == "DEBOUT";
}


public boolean estInterieur() {
	return this.monEtat != "DEHORS";
}


@Override
public String toString() {
	return "<" + monEtat + ">";
}


}
